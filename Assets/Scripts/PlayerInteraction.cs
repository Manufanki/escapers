using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInteraction : MonoBehaviour
{
    [SerializeField] float length = 100;
    
    List<Key> keyList;

    // Start is called before the first frame update
    void Start()
    {
        keyList = new List<Key>();
    }

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward)* length, Color.red);
        if(Input.GetMouseButtonDown(0))
        {
            if(Physics.Raycast(Camera.main.transform.position, Camera.main.transform.TransformDirection(Vector3.forward), out hit, length))
            {
                if(hit.transform.tag == "Key")
                {
                    Debug.Log("Key collected");
                    Key key = hit.transform.GetComponent<Key>();
                    AddKeyToList(key);
                    Main._instance.myUiManger.ShowKeyUI(key.keyIndex);
                }

                if(hit.transform.tag == "Door")
                {
                    Debug.Log("Try to unlock");
                    DoorOpener door = hit.transform.GetComponent<DoorOpener>();

                    if(keyList.Count == 0)
                    {
                        Main._instance.myUiManger.ShowMissingKeyUI();
                    }
                    else
                        door.Unlock(keyList);
                }
            }
        }
    }

    public void AddKeyToList(Key k)
    {
        if(keyList.Contains(k))
        {
            return;
        }

        keyList.Add(k);
    }
}
