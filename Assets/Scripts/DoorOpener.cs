using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoorOpener : MonoBehaviour
{
    public GameObject doorRotator;
    public GameObject missingKeyUI;
    public bool locked = true;
    public int keyIndex = -1;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void Unlock(List<Key> keyList)
    {
        Debug.Log(keyIndex);

        bool keyFound = false;

        foreach(var item in keyList)
        {
            if(item.keyIndex == keyIndex)
            {
                keyFound = true;
                locked = false;
                doorRotator.GetComponent<Animator>().SetBool("Open", true);
                Main._instance.myUiManger.ShowDoorUI(item.keyIndex);
            }
        }

        if(!keyFound)
        { 
            Main._instance.myUiManger.ShowMissingKeyUI();
        }
    }
}
