using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] CanvasGroup missingKeyUI;
    [SerializeField] CanvasGroup startUI;
    [SerializeField] CanvasGroup endUI;
    [SerializeField] CanvasGroup orangeKeyUI;
    [SerializeField] CanvasGroup orangeDoorUI;
    [SerializeField] CanvasGroup blueKeyUI;
    [SerializeField] CanvasGroup blueDoorUI;
    [SerializeField] CanvasGroup purpleKeyUI;
    [SerializeField] CanvasGroup purpleDoorUI;
    [SerializeField] RawImage orangeKey;
    [SerializeField] RawImage blueKey;
    [SerializeField] RawImage purpleKey;

    CanvasGroup animatedGroup;

    void Awake()
    {
    }

    void Start() 
    {
        Main._instance.myUiManger = this;
        ShowStartUI();
    }

    public void ShowStartUI()
    {
        startUI.gameObject.SetActive(true);
        CheckAnimation();
        StartCoroutine(FadeOutImage(10f, startUI));
    }

    public void ShowEndUI()
    {
        endUI.gameObject.SetActive(true);
        CheckAnimation();
        StartCoroutine(FadeOutImage(5f, endUI));
    }

    public void ShowMissingKeyUI()
    { 
     missingKeyUI.gameObject.SetActive(true);
     CheckAnimation();
     StartCoroutine(FadeOutImage(5f, missingKeyUI));  
    }

    public void ShowDoorUI(int doorIndex)
    {
        if(doorIndex == 1)
        {
            orangeDoorUI.gameObject.SetActive(true);
            CheckAnimation();
            StartCoroutine(FadeOutImage(5f, orangeDoorUI));
        }
        
        if(doorIndex == 2)
        {
            blueDoorUI.gameObject.SetActive(true);
            CheckAnimation();
            StartCoroutine(FadeOutImage(5f, blueDoorUI));
        }

        if(doorIndex == 3)
        {
            purpleDoorUI.gameObject.SetActive(true);
            CheckAnimation();
            StartCoroutine(FadeOutImage(5f, purpleDoorUI));
        }
    }

    public void ShowKeyUI(int keyIndex)
    {
        if(keyIndex == 1)
        {
            orangeKeyUI.gameObject.SetActive(true);
            CheckAnimation();
            StartCoroutine(FadeOutImage(5f, orangeKeyUI));
            orangeKey.gameObject.SetActive(true);
        }

        if(keyIndex == 2)
        {
            blueKeyUI.gameObject.SetActive(true);
            CheckAnimation();
            StartCoroutine(FadeOutImage(5f, blueKeyUI));
            blueKey.gameObject.SetActive(true);
        }

        if(keyIndex == 3)
        {
            purpleKeyUI.gameObject.SetActive(true);
            CheckAnimation();
            StartCoroutine(FadeOutImage(5f, purpleKeyUI));
            purpleKey.gameObject.SetActive(true);
        }
    }

    IEnumerator FadeOutImage(float time, CanvasGroup image){
        animatedGroup = image;
        yield return new WaitForSeconds(time);

        float tmpTime = 2;

        while ( tmpTime > 0)
        {
             image.alpha = tmpTime/2;
             tmpTime -= Time.fixedDeltaTime;
             yield return new WaitForFixedUpdate();
        }
        image.gameObject.SetActive(false);
        image.alpha = 1;
        animatedGroup = null;
    }

    public void CheckAnimation()
    {
        if(!animatedGroup)
            return;     
        StopAllCoroutines();
        animatedGroup.alpha = 1;
        animatedGroup.gameObject.SetActive(false);
    }
}
